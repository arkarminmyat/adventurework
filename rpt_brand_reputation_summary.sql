CREATE TABLE 'rpt_brand_reputation_summary'
SELECT AA.brand_name AS BRAND,
	   SUM(AA.QTY) AS SOLD_OUT_QTY,
	   SUM(AA.TOTAL_ORG_AMT) AS NET_INCOME,
	   SUM(AA.TOTAL_DIS_AMT) AS NET_DIS,
	   'USD' AS CCY
	   FROM
(SELECT A.*,C.brand_name FROM
(SELECT product_id,SUM(quantity) AS QTY,SUM(ORG_AMT) AS TOTAL_ORG_AMT,
		SUM(DIS_AMT) AS TOTAL_DIS_AMT FROM
(SELECT product_id,quantity,CAST(ORG_AMT AS INTEGER) AS ORG_AMT,
		CAST(DIS_AMT AS INTEGER) AS DIS_AMT FROM
(SELECT order_id,product_id,quantity,
	   (list_price - (list_price * discount)) * quantity AS DIS_AMT,
	   list_price * quantity AS ORG_AMT
FROM sale_order_item_master
WHERE order_id IN (SELECT order_id FROM sale_order_details
WHERE order_status = 4
)))
GROUP BY product_id) A
LEFT JOIN (SELECT product_id,brand_id,model_year FROM prod_product) B
ON A.product_id = B.product_id
LEFT JOIN (SELECT brand_id,brand_name FROM prod_brand) C
ON B.brand_id = C.brand_id) AA
GROUP BY brand_name