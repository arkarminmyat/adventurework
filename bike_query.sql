--WHEN 1 THEN 'Pending'
--WHEN 2 THEN 'Processing'
--WHEN 3 THEN 'Rejected'
--WHEN 4 THEN 'Completed'

-- rpt_order_summary
CREATE TABLE rpt_order_summary AS
SELECT ORDER_ID,TOTAL_QTY,TOTAL_ORG_AMT,TOTAL_DIS_AMT,TOTAL_AMI_VARI,
	   CASE order_status 
	   WHEN '1' THEN 'PENDING'
	   WHEN '2' THEN 'PROCESSING'
	   WHEN '3' THEN 'REJECTED'
	   WHEN '4' THEN 'COMPLETED'
	   END AS ORDER_STATUS FROM
(SELECT A.ORDER_ID,A.TOTAL_QTY,A.TOTAL_ORG_AMT,A.TOTAL_DIS_AMT,A.TOTAL_AMI_VARI,B.order_status FROM
(SELECT ORDER_ID,
       SUM(QUANTITY) AS TOTAL_QTY,
	   SUM(ORG_AMT) AS TOTAL_ORG_AMT,
	   SUM(DIS_AMT) AS TOTAL_DIS_AMT,
	   SUM(AMT_VARI) AS TOTAL_AMI_VARI FROM
(SELECT ORDER_ID,QUANTITY,ORG_AMT,DIS_AMT,(ORG_AMT - DIS_AMT) AS AMT_VARI FROM
(SELECT order_id AS ORDER_ID,
	   quantity AS QUANTITY,
	   (list_price * quantity) AS ORG_AMT,
	   ((list_price - (list_price * discount)) * quantity) AS DIS_AMT 
	   FROM
(SELECT order_id,quantity,list_price,discount
FROM sale_order_item_master
--WHERE order_id in (SELECT order_id FROM sale_order_details WHERE order_status in ('2','4'))
)))
GROUP BY ORDER_ID) A
LEFT JOIN (SELECT order_id,order_status FROM sale_order_details) B
ON B.order_id = A.order_id)

-- rpt_product_summary
CREATE TABLE 'rpt_product_summary' AS
SELECT PRODUCT,MODEL_YEAR,SOLD_QTY,EXPECT_INCOME,ACTUAL_INCOME,NET_LOSS FROM(
SELECT B.product_name AS PRODUCT,B.model_year AS MODEL_YEAR,B.list_price,
	   A.TOTAL_QTY AS SOLD_QTY,
	   A.TOTAL_ORG_AMT AS EXPECT_INCOME,
	   A.TOTAL_DIS_AMT AS ACTUAL_INCOME,
	   A.TOTAL_AMI_DIFF AS NET_LOSS
	   FROM
(SELECT product_id,
	   CAST(SUM(quantity) AS INTEGER) AS TOTAL_QTY,
	   CAST(SUM(ORG_AMT) AS INTEGER) AS TOTAL_ORG_AMT,
	   CAST(SUM(DIS_AMT) AS INTEGER) AS TOTAL_DIS_AMT,
	   CAST(SUM(AMT_DIFF) AS INTEGER) AS TOTAL_AMI_DIFF FROm
(SELECT product_id,quantity,ORG_AMT,DIS_AMT,(ORG_AMT - DIS_AMT) AS AMT_DIFF FROM
(SELECT product_id,quantity,(list_price * quantity) AS ORG_AMT,
	   ((list_price - (list_price * discount)) * quantity) AS DIS_AMT
FROM
(SELECT order_id,product_id,quantity,list_price,discount
FROM sale_order_item_master
WHERE order_id in (SELECT order_id FROM sale_order_details WHERE order_status = '4')
)))
GROUP BY product_id) A
LEFT JOIN (SELECT product_id,product_name,model_year,list_price FROM prod_product) B
ON B.product_id = A.product_id)
ORDER BY NET_LOSS DESC

CREATE TABLE 'rpt_fiyear_order_summary' AS
SELECT FI_YEAR,SUM(ORDER_ITEM) AS TOTAL_ORDER,SUM(ORDER_ITEM_COUNT) AS TOTAL_ORDER_ITEM FROM
(SELECT A.order_id,substr(A.order_date,0,5) AS FI_YEAR,B.ORDER_ITEM,B.ORDER_ITEM_COUNT FROM
(SELECT order_id,order_status,order_date
FROM sale_order_details
WHERE order_status = 4) A
LEFT JOIN (SELECT order_id,count(order_id) AS ORDER_ITEM,sum(quantity) AS ORDER_ITEM_COUNT
from sale_order_item_master
GROUP BY order_id) B
ON A.order_id = B.order_id)

CREATE TABLE 'rpt_fiyear_fimonth_ord_amt_details' AS
SELECT A.order_id,A.FI_YEAR,A.FI_MONTH,B.ORD_CNT,B.ORD_ITM_CNT,B.SOLD_AMT FROM
(SELECT order_id,FI_YEAR,
	   CASE WHEN FI_MONTH = '01' THEN 'JAN'
			WHEN FI_MONTH = '02' THEN 'FEB'
			WHEN FI_MONTH = '03' THEN 'MAR'
			WHEN FI_MONTH = '04' THEN 'APR'
			WHEN FI_MONTH = '05' THEN 'MAY'
			WHEN FI_MONTH = '06' THEN 'JUN'
			WHEN FI_MONTH = '07' THEN 'JUL'
			WHEN FI_MONTH = '08' THEN 'AUG'
			WHEN FI_MONTH = '09' THEN 'SEP'
			WHEN FI_MONTH = '10' THEN 'OCT'
			WHEN FI_MONTH = '11' THEN 'NOV'
			WHEN FI_MONTH = '12' THEN 'DEC'
			
	   END AS FI_MONTH FROM
(SELECT order_id,order_status,substr(order_date,0,5) AS FI_YEAR,
	   substr(order_date,6,2) AS FI_MONTH
FROM sale_order_details
WHERE order_status in ('4')
)) A
LEFT JOIN (SELECT order_id,
	   count(order_id) AS ORD_CNT,
	   sum(quantity) AS ORD_ITM_CNT,
	   CAST(sum((list_price - (list_price*discount)) * quantity) AS INTEGER) AS SOLD_AMT
FROM sale_order_item_master
GROUP BY order_id) B
ON A.order_id = B.order_id

CREATE TABLE 'rpt_fiyear_fiquarter_ord_amt_details' AS
SELECT FI_YEAR,
	   FI_QUARTER,
	   SUM(ORD_CNT) AS TOTAL_ORD_CNT,
	   SUM(ORD_ITM_CNT) AS TOTAL_ORD_ITM_CNT,
	   SUM(SOLD_AMT) AS TOTAL_SOLD_AMT,
	   'USD' AS CCY
	   FROM
(SELECT FI_YEAR,ORD_CNT,ORD_ITM_CNT,SOLD_AMT,
       CASE WHEN FI_MONTH in ('JAN','FEB','MAR') THEN 'Q1'
			WHEN FI_MONTH in ('APR','MAY','JUN') THEN 'Q2'
			WHEN FI_MONTH in ('JUL','AUG','SEP') THEN 'Q3'
			WHEN FI_MONTH in ('OCT','NOV','DEC') THEN 'Q4'
	   END AS FI_QUARTER FROM
(SELECT A.order_id,A.FI_YEAR,A.FI_MONTH,B.ORD_CNT,B.ORD_ITM_CNT,B.SOLD_AMT FROM
(SELECT order_id,FI_YEAR,
	   CASE WHEN FI_MONTH = '01' THEN 'JAN'
			WHEN FI_MONTH = '02' THEN 'FEB'
			WHEN FI_MONTH = '03' THEN 'MAR'
			WHEN FI_MONTH = '04' THEN 'APR'
			WHEN FI_MONTH = '05' THEN 'MAY'
			WHEN FI_MONTH = '06' THEN 'JUN'
			WHEN FI_MONTH = '07' THEN 'JUL'
			WHEN FI_MONTH = '08' THEN 'AUG'
			WHEN FI_MONTH = '09' THEN 'SEP'
			WHEN FI_MONTH = '10' THEN 'OCT'
			WHEN FI_MONTH = '11' THEN 'NOV'
			WHEN FI_MONTH = '12' THEN 'DEC'
			
	   END AS FI_MONTH FROM
(SELECT order_id,order_status,substr(order_date,0,5) AS FI_YEAR,
	   substr(order_date,6,2) AS FI_MONTH
FROM sale_order_details
WHERE order_status in ('4')
)) A
LEFT JOIN (SELECT order_id,
	   count(order_id) AS ORD_CNT,
	   sum(quantity) AS ORD_ITM_CNT,
	   CAST(sum((list_price - (list_price*discount)) * quantity) AS INTEGER) AS SOLD_AMT
FROM sale_order_item_master
GROUP BY order_id) B
ON A.order_id = B.order_id))
GROUP BY FI_YEAR,FI_QUARTER



